const { Users, Biodatas } = require("../../models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

exports.login = async (req, res) => {
  // res.send("Login berhasil");
  const { username, password } = req.body;
  const user = await Users.findOne({
    where: { username },
    include: Biodatas
  });

  if (!user) {
    res.status(402).json({
        result: "Failed",
        message: "username not found!"
    });
  } else {
    validPass = await bcrypt.compare(password, user.password);
    if (!validPass) res.status(401).json({
      result: "Failed",
      message: "password incorrect!",
    });
    else {
      const { email, username, Biodata, id } = user;
      const {name, gender, address,user_id, social_media_url} = Biodata
      const token = jwt.sign(
        {
          email,
          username,
          name,
          gender,
          address,
          user_id,
          social_media_url,
          id,
        },
        "secret"
      );

      const response = {
        name: name,
        accessToken: token,
      };
      res.send(response);
    }
  }
};
