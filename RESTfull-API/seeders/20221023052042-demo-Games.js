'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
   return queryInterface.bulkInsert("Games", [
     {
       name: "Rock Paper Scissor",
       description: "Game klasik dengan memilih dari 3 pilihan antara ROCK PAPER atau SCISSOR",
       game_url: "dummy",
       createdAt: new Date(),
       updatedAt: new Date(),
     }
   ]);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Games", null, {});
  }
};
