const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const passport = require("./lib/passport");
const authenticationController = require("./controller/authentication");

const app = express();
const port = 8082;

app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());

app.post("/login", authenticationController.login);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
