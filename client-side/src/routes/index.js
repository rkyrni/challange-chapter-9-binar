import React from "react";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import Landing from "../pages/Landing";
import Login from "../pages/Login";
import StateManagement from "../stateManagement";

const Router = () => {
  return (
    <BrowserRouter>
      <StateManagement>
        <Routes>
          <Route path="/" element={<Landing />} />
          <Route path="/login" element={<Login />} />
        </Routes>
      </StateManagement>
    </BrowserRouter>
  );
};

export default Router;
