import "./App.css";
import Router from "./routes";
require("bootstrap/dist/css/bootstrap.min.css");

const App = () => {
  return <Router />;
};

export default App;
