import { createContext, useState } from "react";

export const DataContext = createContext();

const StateManagement = (props) => {
  const [sideBarCollapse, setSideBarCollapse] = useState(false);
  const [user, setUser] = useState(null);
  const [token, setToken] = useState(null);
  return (
    <DataContext.Provider
      value={{
        sideBarCollapse,
        setSideBarCollapse,
        user,
        setUser,
        token,
        setToken,
      }}
    >
      {props.children}
    </DataContext.Provider>
  );
};

export default StateManagement;
