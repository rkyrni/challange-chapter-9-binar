import axios from "axios";
import React, { useContext, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import LogoLogin from "../../assets/img/logo-2.png";
import "../../assets/style/login.css";
import { DataContext } from "../../stateManagement";

const Login = () => {
  const { setUser, setToken } = useContext(DataContext);
  const [textHeader, setTextHeader] = useState("Log in");
  const [isWrongInput, setIsWrongInput] = useState(false);
  const [input, setInput] = useState({
    username: "",
    password: "",
  });

  const navigate = useNavigate();

  const handlerChangeInput = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handlerSubmitLogin = async (e) => {
    e.preventDefault();
    try {
      const user = await axios.post("http://localhost:8082/login", input);
      setUser(user.data.name);
      setToken(user.data.accessToken);
      navigate("/");
      setInput({
        username: "",
        password: "",
      });
    } catch (error) {
      //   console.log(error.response.status);
      if (error.response.status === 402) {
        setTextHeader("Username Incorrect!");
        setIsWrongInput(true);
      }
      if (error.response.status === 401) {
        setTextHeader("Password Incorrect!");
        setIsWrongInput(true);
      }
    }
  };

  return (
    <div className="wrapper">
      <div className="logo">
        <img src={LogoLogin} alt="logo" />
      </div>
      <div
        className={`text-center mt-4 name ${isWrongInput ? "text-danger" : ""}`}
      >
        {textHeader}
      </div>
      <form
        method="POST"
        action="POST"
        onSubmit={handlerSubmitLogin}
        className="p-3 mt-3"
      >
        <div className="form-field d-flex align-items-center">
          <span className="far fa-user" />
          <input
            type="text"
            name="username"
            value={input.username}
            onChange={handlerChangeInput}
            id="userName"
            placeholder="Username"
          />
        </div>
        <div className="form-field d-flex align-items-center">
          <span className="fas fa-key" />
          <input
            type="password"
            name="password"
            value={input.password}
            onChange={handlerChangeInput}
            id="pwd"
            placeholder="Password"
          />
        </div>
        <button type="submit" className="btn mt-3">
          Login
        </button>
      </form>
      <div className="text-center fs-6">
        <Link to="#">Forget password?</Link> or <Link to="#">Sign up</Link>
      </div>
    </div>
  );
};

export default Login;
