import React from "react";
import Main from "./Main";
import "../../assets/style/landing.css";
import GameChoice from "./GameChoice";

const Landing = () => {
  return (
    <div className="container-fluid p-0">
      <Main />
      <GameChoice />
    </div>
  );
};

export default Landing;
