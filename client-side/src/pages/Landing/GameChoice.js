import React from "react";
import ImgCarousel1 from "../../assets/img/rockpaperstrategy-1600.jpg";
import ImgCarousel2 from "../../assets/img/rockpaperstrategy-1700.jpg";
import ImgCarousel3 from "../../assets/img/rockpaperstrategy-1800.jpg";
import ImgCarousel4 from "../../assets/img/rockpaperstrategy-1900.jpg";
import ImgCarousel5 from "../../assets/img/rockpaperstrategy-2000.jpg";

const GameChoice = () => {
  return (
    <section
      id="game-choice"
      className="background-set d-flex flex-lg-row flex-column text-white justify-content-lg-center align-items-center align-items-lg-start"
    >
      <div
        id="game-choice-content"
        className="col-lg-3 d-flex flex-column col-sm-5 ps-4 pt-md-0"
      >
        <span className="fs-4 fw-light">What's so special?</span>
        <h2 className>THE GAMES</h2>
      </div>
      {/* CAROUSEL */}
      <div className="col-7">
        <div
          id="carouselExampleIndicators"
          className="carousel slide"
          data-bs-ride="true"
        >
          <div className="carousel-indicators">
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to={0}
              className="btn-slide-carousel active"
              aria-current="true"
              aria-label="Slide 1"
            />
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to={1}
              className="btn-slide-carousel"
              aria-label="Slide 2"
            />
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to={2}
              className="btn-slide-carousel"
              aria-label="Slide 3"
            />
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to={3}
              className="btn-slide-carousel"
              aria-label="Slide 4"
            />
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to={4}
              className="btn-slide-carousel"
              aria-label="Slide 5"
            />
          </div>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img
                src={ImgCarousel1}
                className="d-block w-100"
                alt="pic-Carousel"
              />
            </div>
            <div className="carousel-item">
              <img
                src={ImgCarousel2}
                className="d-block w-100"
                alt="pic-Carousel"
              />
            </div>
            <div className="carousel-item">
              <img
                src={ImgCarousel3}
                className="d-block w-100"
                alt="pic-Carousel"
              />
            </div>
            <div className="carousel-item">
              <img
                src={ImgCarousel4}
                className="d-block w-100"
                alt="pic-Carousel"
              />
            </div>
            <div className="carousel-item">
              <img
                src={ImgCarousel5}
                className="d-block w-100"
                alt="pic-Carousel"
              />
            </div>
          </div>
          {/* === BUTTON PREV - NEXT === */}
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev"
          >
            <svg
              aria-hidden="true"
              width={20}
              height={38}
              viewBox="0 0 20 38"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M18 1.70007L18 35.7001L2 18.8904L18 1.70007Z"
                stroke="white"
                strokeWidth="3.36454"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
            <span className="visually-hidden">Previous</span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next"
          >
            <svg
              aria-hidden="true"
              width={18}
              height={39}
              viewBox="0 0 18 39"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M1.82153 2.65635L1.82153 36.7297L15.5642 19.8838L1.82153 2.65635Z"
                stroke="white"
                strokeWidth="3.36454"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
            <span className="visually-hidden">next</span>
          </button>
          {/* === END BUTTON PREV - NEXT === */}
        </div>
      </div>
      {/* END CAROUSEL */}
    </section>
  );
};

export default GameChoice;
